These files can be used as a postfix map like this:

smtp_tls_policy_maps = btree:/etc/postfix/tls_policy_extern
<br>
btree:/etc/postfix/tls_policy_fraunhofer
<br>
btree:/etc/postfix/tls_policy_helmholtz
<br>
btree:/etc/postfix/tls_policy_leibniz
<br>
btree:/etc/postfix/tls_policy_mpg

For more information check this out:
http://www.postfix.org/TLS_README.html#client_tls_policy
